var models  = require('../app/models/index');
var express = require('express'),
    router  = express.Router();

router.get('/', (req, res) => {
  res.render('../app/views/index.ejs');
});

router.post('/setPoll', (req, res) => {
  var poll = new models.Poll({
    title: req.body.title,
  });

  poll.save((err, poll) => {
    if (err) {
      console.log(err);
      res.send({'ok':'false', 'error':'An error has occurred'});
      return;
    }
    res.status(200).send({'ok':'true'});
  });
});

router.post('/setVote', (req, res) => {
  models.Poll.findOne((err, poll) => {
    if (err) {
      console.log(err);
      res.send({'ok':'false', 'error':'An error has occurred'});
      return;
    }
    var poll_id = poll['_id'];
    var vote = new models.Vote({
      poll_id: poll_id,
      title: req.body.title,
      image: req.body.image
    });
    vote.save((err, vote) => {
      if (err) {
        console.log(err);
        res.send({'ok':'false', 'error':'An error has occurred'});
        return;
      }
      res.status(200).send(vote);
    });
  }).sort({created_at: -1});
});

router.get('/getPollVotes', (req, res) => {
  models.Poll.findOne((err, poll) => {
    if (err) {
      console.log(err);
      res.send({'ok':'false', 'error':'An error has occurred'});
      return;
    }
    models.Vote.find({poll_id: poll['_id']}, (err, votes) => {
      poll_votes = {
        ok:'true',
        _id: poll['_id'],
        title: poll['title'],
        created_at: poll['created_at'],
        votes: votes
      }
      res.status(200).send(poll_votes);
    });
  }).sort({created_at: -1});
});

router.post('/setVotePoll', (req, res) => {
  var poll_vote = new models.PollVote({
    poll_id: req.body.poll_id,
    vote_id: req.body.vote_id,
    ip:      req.body.ip,
    os:      req.body.os,
    country: req.body.country
  });
  poll_vote.save((err) => {
    if (err) {
      console.log(err);
      res.send({'ok':'false', 'error':'An error has occurred'});
      return;
    }
    res.status(200).send({'ok':'true'});
  });
});

module.exports = {rout: router};