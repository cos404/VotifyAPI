/*
 * @module models
 * @author Maxim Hvaschinsky 22division7@gmail.com
 * @license MIT
 */

const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;
const pollSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    }
  },
  {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}
  }
);

module.exports = mongoose.model('poll', pollSchema);