/*
 * @module models
 * @author Maxim Hvaschinsky 22division7@gmail.com
 * @license MIT
 */

const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;
const voteSchema = new Schema({
  poll_id: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true
  },
  count: {
    type: Number,
    default: 0
  }
});

voteSchema.index({poll_id: 1, title: 1},{unique: true});

module.exports = mongoose.model('vote', voteSchema);