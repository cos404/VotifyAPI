/*
 * Includes all models into one place
 * @module models
 * @author Maxim Hvaschinsky 22division7@gmail.com
 * @license MIT
 */

module.exports = {
  Poll:     require('./poll.js'),
  PollVote: require('./poll_vote.js'),
  Vote:     require('./vote.js'),
};