/*
 * @module models
 * @author Maxim Hvaschinsky 22division7@gmail.com
 * @license MIT
 */

const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;
const pollVoteSchema = new Schema({
  poll_id: {
    type: String,
    required: true,
  },
  vote_id: {
    type: String,
    required: true,
  },
  ip: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  os: {
    type: String,
    required: true,
  }
});

pollVoteSchema.index({ip: 1, poll_id: 1},{unique: true});

module.exports = mongoose.model('poll_vote', pollVoteSchema);