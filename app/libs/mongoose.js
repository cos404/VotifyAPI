var mongoose = require('mongoose');
var config = require('../../configs/index');
mongoose.connect(config.mongoose.uri);
module.exports = mongoose;