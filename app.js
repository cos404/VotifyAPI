var express     = require('express'),
    app         = express();
var router      = require('./configs/routes.js');
var bodyParser  = require('body-parser');

var config  = require('./configs/index'),
    models  = require('./app/models/index');

app.use(express.static(__dirname + '/app'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Origin', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});


app.use('/', router.rout);

app.listen(config.port, function(){
  console.log('App running on port: ' + config.port);
});